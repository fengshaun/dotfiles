// ==UserScript==
// Armin M.
// @name        9gag uncluttered
// @namespace   9gag
// @include     *9gag.com/*
// @version     1
// @grant       none
// ==/UserScript==

jQuery(".post-info").css("display", "none");
jQuery(".shares").css("display", "none");
jQuery("#sidebar").css("display", "none");
jQuery(".footer-static").css("display", "none");
jQuery(".read-next").css("display", "none");

jQuery(".badge-item-title").css("position", "absolute");
jQuery(".badge-item-title").css("top", "0px");
jQuery(".badge-item-title").css("right", "-300px");
jQuery(".badge-item-title").css("width", "290px");

jQuery(".float-vote-shadow").css("position", "absolute");
jQuery(".float-vote-shadow").css("top", "120px");
jQuery(".float-vote-shadow").css("right", "-60px");
jQuery(".float-vote-shadow").css("width", "60px");

jQuery(".badge-facebook-share").css("display", "none");
