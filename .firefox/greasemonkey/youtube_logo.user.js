// ==UserScript==
// @name        youtube_logo
// @namespace   youtube_logo
// @description changes youtube's logo href to subscripts > upload
//              and redirects youtube.com to subscription page
// @include     *www.youtube.com*
// @version     0.0.1
// @grant       none
// ==/UserScript==

var logo = document.getElementById("logo-container");
logo.href = "/feed/subscriptions/u";

if (document.URL == "https://www.youtube.com/" || document.URL == "http://www.youtube.com/") {
  window.location.href = "https://www.youtube.com/feed/subscriptions/u";
}