IFS=":"

while read -r line; do
  case $line in 
    M*)
      m="\uf001 ${line#?}"
      ;;
    T*)
      t="\uf274 ${line#?}"
      ;;
	L*)
	  l="\uf0e0 ${line#?}"
	  ;;
    W*)
      wm="%{U#1793d1}  %{U-} "
      IFS=":"
      for desktop in $line; do
        case $desktop in
          o*)
            # occupied unfocused
            wm="$wm%{U#504945}%{+u}          %{-u}%{U-} "
            ;;
          f*)
            # free unfocused
            wm="$wm           "
            ;;
          [OFU]*)
            # occupied/free focused
            wm="$wm%{U#ebdbb2}%{+u}          %{-u}%{U-} "
            ;;
          u*)
            # urgent unfocused
            wm="$wm%{U#fb4934}%{+u}          %{-u}%{U-} "
            ;;
          *)
            ;;
        esac
      done
      wm="$wm %{U#1793d1}  %{U-}"
    esac
    echo -e "%{l}$m%{c}$wm%{r}$l  $t"
done

