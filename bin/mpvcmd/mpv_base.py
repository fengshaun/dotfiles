import os, sys
import json
import socket
import time
import subprocess

from contextlib import contextmanager

sock_path = "/tmp/mpv.socket"

@contextmanager
def mpv_sock():
    try: 
        sock = None
        if os.path.exists(sock_path):
            print("does not exist")
            sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)

            for x in range(3):
                try:
                    print("trying to connect", file=sys.stderr)
                    sock.connect(sock_path)
                    break
                except ConnectionRefusedError:
                    print("refused", file=sys.stderr)
                    subprocess.Popen(["mpv"])
                    print("sleeping", file=sys.stderr)
                    time.sleep(2)
        yield sock

    finally:
        if sock is not None:
            sock.close()

class Mpv(object):
    def __init__(self, sock):
        self.sock = sock
        
    def send(self, msg):
        self.sock.send(msg)

    def recv(self):
        reply = ""
        while True:
            data = self.sock.recv(1024)
            reply += data.decode("utf8")

            if len(data) < 1024:
                break

        return reply
