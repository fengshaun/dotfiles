#!/bin/sh

PANEL_FIFO=~/lemonbar.fifo

[ -p $PANEL_FIFO ] || mkfifo $PANEL_FIFO

# time
while true; do
  echo "T$(date +%c)"
  sleep 1
done > $PANEL_FIFO &

while true; do
  echo "L$(find ~/mail/inbox/new -type f | wc -l)"
  sleep 5
done > $PANEL_FIFO &

bspc subscribe > $PANEL_FIFO &

#trap 'kill -TERM -- -$(ps x -o "%r %c" | grep bar.sh | uniq | sed -e "s:^ *::g" | cut -d" " -f1)' INT TERM QUIT EXIT

/home/armin/bin/parse_bar.sh < $PANEL_FIFO | /home/armin/pkgs/bar/lemonbar -g 2560x16x0x0 -p -n lemonbar -u 3 -B "#282828" -F "#ebdbb2" -f "FontAwesome-8" -o -1 -f "Fantasque Sans Mono-9" -o 1

