#!/bin/sh

rating=0
stationName=""

IFS="="
while read -r left right; do
	case $left in
		"rating")
			if [ "$right" = "1" ]; then
				rating="1"
			fi
			;;
		"stationName")
			stationName="$right"
			;;
	esac
done

if [ $stationName = "Half Hitz" ]; then exit 1; fi
if [ $rating = "1" ]; then exit 0; fi
exit 1
