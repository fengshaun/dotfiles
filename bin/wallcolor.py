#!/usr/bin/python2

import sys
import colorsys
from colorz import colorz

def get_nitrogen_bg():
  with open("/home/armin/.config/nitrogen/bg-saved.cfg") as f:
    return [line for line in f.readlines() if line.startswith("file")][0].partition("=")[2].rstrip()

def normalize(hexv, minv=128, maxv=256):
  hexv = hexv[1:]
  r, g, b = (
      int(hexv[0:2], 16) / 256.0,
      int(hexv[2:4], 16) / 256.0,
      int(hexv[4:6], 16) / 256.0,
      )
  h, s, v = colorsys.rgb_to_hsv(r, g, b)
  minv = minv / 256.0
  maxv = maxv / 256.0
  if v < minv:
    v = minv
  if v > maxv:
    v = maxv
  r, g, b = colorsys.hsv_to_rgb(h, s, v)
  return '{:02x}{:02x}{:02x}'.format(int(r * 256), int(g * 256), int(b * 256))

if __name__ == '__main__':
  if len(sys.argv) == 1:
    n = 16
  else:
    n = int(sys.argv[1])

  WALLPAPER = get_nitrogen_bg()
  termite = """[colors]
foreground = #eaeaea
background = #151515
"""
  bar = """
#define FOREGROUND 0xeaeaea
#define BACKGROUND 0x151515
"""

  i = 0
  with open('colorz.html', 'w') as f:
    f.write("""<img src="file://{}" height=200/>""".format(WALLPAPER))
    for c in colorz(WALLPAPER, n=n):
      if i == 0:
        c = normalize(c, minv=0, maxv=32)
      elif i == 8:
        c = normalize(c, minv=128, maxv=192)
      elif i < 8:
        c = normalize(c, minv=160, maxv=224)
      else:
        c = normalize(c, minv=200, maxv=256)
      f.write("""
          <div style="background-color: {0}; width: 100%; height: 50px">{1}: {0}</div>
          """.format(c, i)
      )

      termite += """color{} = #{}\n""".format(i, c)
      bar += """#define COLOR{} 0x{}\n""".format(i, c)
      i += 1

  print "TERMITE COLORS"
  print termite
  print

  print "BAR-AINT-RECURSIVE COLORS"
  print bar 

