#!/usr/bin/python

import sys, re

### Some global files ###

status_file = "/home/armin/.pandora_status"
debug_file = "/home/armin/.pandora_debug"

### Parsing and basic output ###

def parse_info(l):
    """ artist
        title
        album
        coverArt (url)
        stationName
        songStationName
        pRet 
        pRetStr (e.g. "Everything is fine :)")
        wRet
        wRetStr
        songDuration
        songPlayed
        rating
        detailUrl (url)
        stationCount
        station{0,1,2,etc.} (e.g. station0=this station1=that)
    """
    d = {i.split('=')[0]: i.split('=')[1] for i in l}

    if int(d["rating"]) > 0:
        d["love"] = "<3"
        d["loved"] = "yes"
    else:
        d["love"] = ""
        d["loved"] = "no"

    return d

def report(i):
    return "%(title)s - %(artist)s %(love)s\n" % i

### Actions done on the status ###

def write_status(info):
    with open(status_file, 'w') as f:
        f.write(report(parse_info(info)))

def change_status(before, after):
    """ changes the first line of the status file
        according to "before" and "after" strings
    """
    with open(status_file, 'r') as f:
        lines = f.readlines()
        lines[0] = re.sub(before, after, lines[0])

    with open(status_file, 'w') as f:
        [f.write(l) for l in lines]

def love():
    with open(status_file, 'r') as f:
        lines = f.readlines()

        if not re.search('<3', lines[0]):
            lines[0] = lines[0].rstrip() + " <3\n"

    with open(status_file, 'w') as f:
        [f.write(l) for l in lines]

if __name__ == "__main__":
    event = sys.argv[1]
    info = [line.rstrip() for line in sys.stdin.readlines()]

    with open(debug_file, 'w') as f:
        f.write(repr(info))

    if event == "songstart":
        write_status(info)

    if event == "songpause":
        change_status(r"$", " - paused")

    if event == "songplay":
        change_status(r" - paused", "")

    if event == "songlove":
        love()

