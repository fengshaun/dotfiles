#!/usr/bin/python2

import os, sys, string

maildir = os.path.expanduser("~/mail/")
dirs = ["inbox", "facebook", "fwd", "misc"]

total = {}
t = 0

for dir in dirs:
    m = len(os.listdir(maildir + dir + "/new"))
    total[dir] = m
    t += m

for dir in dirs:
   print "%s:${alignr}%s${goto 6} " % (string.capitalize(dir), total[dir])

