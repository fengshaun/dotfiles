#!/usr/bin/python2

import os
from pysqlite2 import dbapi2 as sqlite

max_length = 20
def truncate(str):
    if len(str) > max_length:
        return str[:max_length].rsplit(' ', 1)[0] 
    else:
        return str

cur = sqlite.connect(os.path.expanduser("~/.newsbeuter/cache.db")).cursor()

# find all feeds
cur.execute('select * from rss_feed')
feeds = list(cur)
titles = map(truncate, [row[2] for row in feeds])
urls = [row[0] for row in feeds]

unread_count = []
for url in urls:
    cur.execute('select * from rss_item where feedurl == "%s" and unread == 1' % url)
    unread_count.append(len(list(cur)))

"""
if any(unread_count):
    for i in zip(titles, unread_count):
        if i[1] > 0:
            print "%s:${alignr}%s${goto 6} " % (i[0], i[1])
else:
    print "All:${alignr}0${goto 6} "
"""
print sum(unread_count)
