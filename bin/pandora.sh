#!/bin/sh

PANEL_FIFO=/tmp/lemonbar.fifo

echo "M$1" > $PANEL_FIFO

case "$1" in
    "songstart"|"songplay"|"songpause")
        IFS="="
        while read -r left right; do
            case $left in
                "artist")
                    artist=$right
                    ;;
                "title")
                    title=$right
                    ;;
                "rating")
                    if [ "$right" = "1" ]; then
                    loved="<3"
                    else
                    loved=""
                    fi
                    ;;
            esac
        done
        output="$artist - $title $loved"

        if [ "$1" = "songpause" ]; then
            output="$output - paused"
        fi
        ;;

    "pianoquit")
        output="---"
        ;;
esac

echo "M$output" > $PANEL_FIFO

