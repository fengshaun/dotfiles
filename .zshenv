### environment variables
export PATH=$HOME/pkgs/bin:$HOME/bin:$HOME/bin/mpvcmd:$HOME/pkgs/wmutils/contrib:$HOME/pkgs/dmenu_icase:/mnt/hdd/gradle-4.1/bin:/usr/local/bin:$PATH:/mnt/hdd/.android/tools/bin:/mnt/hdd/.android/tools:/mnt/hdd/.android/platform-tools:/mnt/hdd/.android/emulator/
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib

export EDITOR=emacs-run
export BROWSER=firefox-bin
export TERM="screen-256color"

export STEAM_FRAME_FORCE_CLOSE=1
export MOZ_USE_OMTC=1

export _JAVA_AWT_WM_NONREPARENTING=1
export JAVA_HOME=/usr/lib/jvm/oracle-jdk-bin-1.8/

export ANDROID_HOME=/mnt/hdd/.android
export ANDROID_NDK=/mnt/hdd/.android/ndk-bundle/android-ndk-r16b
export ANDROID_NDK_HOME=/mnt/hdd/.android/ndk-bundle/android-ndk-r16b
