### vi mode
zle -N edit-command-line
zle -N zle-keymap-select
zle -N zle-line-init

bindkey -v
autoload -Uz edit-command-line

bindkey -M vicmd "/" history-incremental-pattern-search-backward
bindkey -M vicmd '^r' redo
bindkey -M vicmd 'G' end-of-buffer-or-history
bindkey -M vicmd 'gg' beginning-of-buffer-or-history
bindkey -M vicmd 'u' undo
bindkey -M vicmd 'v' edit-command-line
bindkey -M vicmd '~' vi-swap-case
bindkey -M vicmd 'k' up-line-or-search
bindkey -M vicmd 'j' down-line-or-search

bindkey -M viins '^r' history-incremental-pattern-search-backward

### movement
setopt auto_cd
setopt auto_pushd
export DIRSTACKSIZE=10
setopt pushd_silent
setopt pushd_ignore_dups

### history
export HISTFILE=~/.zsh-history
export HISTSIZE=5000
export SAVEHIST=3000
export HISTORY_IGNORE="(cd|pushd|pd|popd|exit) *"
setopt hist_ignore_all_dups
setopt hist_expire_dups_first
setopt append_history
setopt extended_history
setopt inc_append_history

### vcs
autoload -Uz vcs_info

precmd() {
  vcs_info
}

zstyle ':vcs_info:*' enable git
zstyle ':vcs_info:*' check-for-changes true
zstyle ':vcs_info:*' formats '%b%c%u'
zstyle ':vcs_info:*' actionformats '%b%c%u|%a'
zstyle ':vcs_info:*' stagedstr '+'
zstyle ':vcs_info:*' unstagedstr '-'

### misc opts
setopt interactivecomments

### aliases
#alias ls="ls --color --indicator-style=file-type"
alias pd=popd

### colors
#eval "`dircolors ~/.dir_colors`"

### completion
# The following lines were added by compinstall

zstyle ':completion:*' auto-description 'specify: %d'
zstyle ':completion:*' completer _expand _complete _ignored _approximate
zstyle ':completion:*' completions 1
zstyle ':completion:*' expand prefix suffix
zstyle ':completion:*' file-sort access
zstyle ':completion:*' format 'completing %d'
zstyle ':completion:*' glob 1
zstyle ':completion:*' group-name ''
zstyle ':completion:*' ignore-parents parent pwd .. directory
zstyle ':completion:*' insert-unambiguous false
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-suffixes true
zstyle ':completion:*' matcher-list '+' '+m:{[:lower:]}={[:upper:]}' '+m:{[:lower:][:upper:]}={[:upper:][:lower:]}' '+r:|[._-]=* r:|=*'
zstyle ':completion:*' max-errors 1
zstyle ':completion:*' menu select=3
zstyle ':completion:*' original true
zstyle ':completion:*' preserve-prefix '//[^/]##/'
zstyle ':completion:*' prompt 'correct %e errors'
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' squeeze-slashes true
zstyle ':completion:*' substitute 1
zstyle ':completion:*' use-compctl false
zstyle ':completion:*' verbose true
zstyle :compinstall filename '$HOME/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

### syntax highlighting
#source ~/pkgs/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
#ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets pattern)
#ZSH_HIGHLIGHT_PATTERNS+=('rm -rf *' 'fg=white,bold,bg=red')

### aliases and convenience functions
alias ls="ls -lG"

jails-run() {
	for j in $(jls host.hostname); do
		echo "[$j] $@"
		jexec $j $@
	done
}

alias poudriere-update-ports="\
/usr/local/bin/poudriere ports -p HEAD -u && \
poudriere-rebuild"

alias poudriere-rebuild="\
/usr/local/bin/poudriere bulk -j freebsd11_x64 -J 1 -p HEAD devel/llvm60 lang/gcc7 && \
/usr/local/bin/poudriere options -j freebsd11_x64 -p HEAD \
    -f /poudriere/ports-list/base \
    -f /poudriere/ports-list/jail.mail \
    -f /poudriere/ports-list/jail.murmur \
    -f /poudriere/ports-list/jail.db \
    -f /poudriere/ports-list/jail.torrent \
    -f /poudriere/ports-list/jail.unifi \
    -f /poudriere/ports-list/jail.web \
    -f /poudriere/ports-list/jail.znc \
    -f /poudriere/ports-list/jail.music \
    -f /poudriere/ports-list/jail.libreoffice_online && \
/usr/local/bin/poudriere bulk -j freebsd11_x64 -p HEAD \
    -f /poudriere/ports-list/base \
    -f /poudriere/ports-list/jail.mail \
    -f /poudriere/ports-list/jail.murmur \
    -f /poudriere/ports-list/jail.db \
    -f /poudriere/ports-list/jail.torrent \
    -f /poudriere/ports-list/jail.unifi \
    -f /poudriere/ports-list/jail.web \
    -f /poudriere/ports-list/jail.znc \
    -f /poudriere/ports-list/jail.music \
    -f /poudriere/ports-list/jail.libreoffice_online"

tmux-list() {
    for d in /tmp/tmux-*/*; do
        uid=$(echo $d | sed -Ee 's:/tmp/tmux-(.+)/.*:\1:g')
        entry=$(getent passwd $uid)
        username=$(echo $entry | perl -pe 's/^(.+?):.*/\1/g')

        echo "$username:"
        IFS=$'\n'; for line in $(tmux -S $d list-sessions 2>&1); do echo "\t$line"; done
        echo
    done
}

### prompt
setopt PROMPT_SUBST
PROMPT='
%F{yellow}[%n@%M]%f %F{blue}%3~%f %F{green}${vcs_info_msg_0_}%f
> '
#RPROMPT="%F{red}$?%f"

